## Results

-----------------------
### Test [input1.bmp](test/input/input1.bmp)
- Time used by asm implementation: 3836 microseconds 
- Time used by C implementation: 27309 microseconds 
- Asm implementation 7 times faster than C 
- Images are equal 
- Successfully created file 
### Source:
![src](test/input/input1.bmp)
### Result (asm impl):
![output_asm](test/output/output_asm_input1.bmp)
### Result (C impl):
![output_asm](test/output/output_c_input1.bmp)
-----------------------
### Test [input2.bmp](test/input/input2.bmp)
- Time used by asm implementation: 19066 microseconds 
- Time used by C implementation: 246393 microseconds 
- Asm implementation 12 times faster than C 
- Images are equal 
- Successfully created file 
### Source:
![src](test/input/input2.bmp)
### Result (asm impl):
![output_asm](test/output/output_asm_input2.bmp)
### Result (C impl):
![output_asm](test/output/output_c_input2.bmp)
-----------------------
### Test [input3.bmp](test/input/input3.bmp)
- Time used by asm implementation: 28009 microseconds 
- Time used by C implementation: 345819 microseconds 
- Asm implementation 12 times faster than C 
- Images are equal 
- Successfully created file 
### Source:
![src](test/input/input3.bmp)
### Result (asm impl):
![output_asm](test/output/output_asm_input3.bmp)
### Result (C impl):
![output_asm](test/output/output_c_input3.bmp)
-----------------------
### Test [input.bmp](test/input/input.bmp)
- Time used by asm implementation: 2 microseconds 
- Time used by C implementation: 1 microseconds 
- Asm implementation 0 times faster than C 
- Images are equal 
- Successfully created file 
### Source:
![src](test/input/input.bmp)
### Result (asm impl):
![output_asm](test/output/output_asm_input.bmp)
### Result (C impl):
![output_asm](test/output/output_c_input.bmp)
-----------------------
### Test [output_expected.bmp](test/input/output_expected.bmp)
- Time used by asm implementation: 1 microseconds 
- Time used by C implementation: 2 microseconds 
- Asm implementation 2 times faster than C 
- Images are equal 
- Successfully created file 
### Source:
![src](test/input/output_expected.bmp)
### Result (asm impl):
![output_asm](test/output/output_asm_output_expected.bmp)
### Result (C impl):
![output_asm](test/output/output_c_output_expected.bmp)
-----------------------
### Test [sample_640×426.bmp](test/input/sample_640×426.bmp)
- Time used by asm implementation: 573 microseconds 
- Time used by C implementation: 5440 microseconds 
- Asm implementation 9 times faster than C 
- Images are equal 
- Successfully created file 
### Source:
![src](test/input/sample_640×426.bmp)
### Result (asm impl):
![output_asm](test/output/output_asm_sample_640×426.bmp)
### Result (C impl):
![output_asm](test/output/output_c_sample_640×426.bmp)
-----------------------
